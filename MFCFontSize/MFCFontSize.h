
// MFCFontSize.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMFCFontSizeApp:
// このクラスの実装については、MFCFontSize.cpp を参照してください。
//

class CMFCFontSizeApp : public CWinApp
{
public:
	CMFCFontSizeApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMFCFontSizeApp theApp;