
// MFCFontSizeDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MFCFontSize.h"
#include "MFCFontSizeDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCFontSizeDlg ダイアログ



CMFCFontSizeDlg::CMFCFontSizeDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCFontSizeDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	// フォントを生成します。
	m_smallFont.CreatePointFont(80, _T("MS ゴシック"));
	m_midiumFont.CreatePointFont(160, _T("MS ゴシック"));
	m_largeFont.CreatePointFont(240, _T("MS ゴシック"));
}

void CMFCFontSizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCFontSizeDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CMFCFontSizeDlg メッセージ ハンドラー

BOOL CMFCFontSizeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// 各ラベルにフォントを設定します。
	GetDlgItem(IDC_LB_SMALL)->SetFont(&m_smallFont);
	GetDlgItem(IDC_LB_MIDIUM)->SetFont(&m_midiumFont);
	GetDlgItem(IDC_LB_LARGE)->SetFont(&m_largeFont);

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CMFCFontSizeDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CMFCFontSizeDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMFCFontSizeDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// フォントの後始末を行います。
	m_smallFont.DeleteObject();
	m_midiumFont.DeleteObject();
	m_largeFont.DeleteObject();
}
